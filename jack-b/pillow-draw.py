# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 13:14:32 2020

@author: jackb
"""
'''
The purpose of this program is to demonstrate an application of the ImageDraw object in the pillow API. 
The script will generate three lemniscates of varying shadings. I couldn't choose which I liked best.
'''
from PIL import Image, ImageDraw, ImageOps
HEIGHT = 300
WIDTH = 500
#Create new image object with RGB constant coloring
im = Image.new('RGB', (WIDTH, HEIGHT), (20, 20, 20))

#Use ImageDraw object to 
draw = ImageDraw.Draw(im)

n = 25
for j in range(3):
    for i in range(n):
        #Method params: (xy, fill, outline) where xy = (x0, y0, x1, y1) and is a rectangle
        #The for loop will disproportionally reduce the size of the ellipse, meanwhile the fill and outline will be a dynamic shading
        draw.ellipse((10*i, 100 + 2*i, 150 + 10*i, 200 - 2*i), fill=(255 - 10*i, 5*i,0), outline=(255- 15*i*j//2, 255- 15*i*j//2, 255- 15*i*j//2))
        draw.ellipse((350 - 10*i, 100 + 2*i, 500 - 10*i, 200 - 2*i), fill=(255- 10*i, 5*i,0), outline=(255- 15*i*j//2, 255- 15*i*j//2, 255- 15*i*j//2))
        
    im_invert = ImageOps.invert(im)  
    im_invert.show()
    im.show()